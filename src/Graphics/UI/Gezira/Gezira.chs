module Graphics.UI.Gezira.Gezira
  (
     Pixels
   , GeziraImage
   , geziraImageSize
   , geziraImageNull
   , geziraImageGate
   , geziraImageSetGate
   , geziraImageSetBounds
   , geziraImagePixels
   , NileProcess
   , nileProcessFromC
   , nileProcessToC
   , nileProcessNull
   , gezira_TransformBeziers
   , gezira_ClipBeziers
   , gezira_CalculateBounds
   , gezira_OffsetBezier
   , gezira_MiterJoin
   , gezira_RoundJoin
   , gezira_JoinBeziers
   , gezira_CapBezier
   , gezira_OffsetAndJoin
   , gezira_StrokeOneSide
   , gezira_ReverseBeziers
   , gezira_SanitizeBezierPath
   , gezira_StrokeBezierPath
   , gezira_DecomposeBeziers
   , gezira_CombineEdgeSamples
   , gezira_Rasterize
   , gezira_RectangleSpans
   , gezira_TransformPoints
   , gezira_PadTexture
   , gezira_RepeatTexture
   , gezira_ReflectTexture
   , gezira_UniformColor
   , gezira_CompositeTextures
   , gezira_ExpandSpans
   , gezira_ExtractSamplePoints
   , gezira_ApplyTexture
   , gezira_SumWeightedColors
   , gezira_BilinearFilterPoints
   , gezira_BilinearFilterWeights
   , gezira_BilinearFilter
   , gezira_BicubicFilterPoints
   , gezira_BicubicFilterDeltas
   , gezira_BicubicFilterWeights
   , gezira_BicubicFilter
   , gezira_GaussianBlur5x1Points
   , gezira_GaussianBlur1x5Points
   , gezira_GaussianBlur5x1Weights
   , gezira_GaussianBlur5x1
   , gezira_GaussianBlur1x5
   , gezira_GaussianBlur11x1Points
   , gezira_GaussianBlur1x11Points
   , gezira_GaussianBlur11x1Weights
   , gezira_GaussianBlur11x1
   , gezira_GaussianBlur1x11
   , gezira_GaussianBlur21x1Points
   , gezira_GaussianBlur1x21Points
   , gezira_GaussianBlur21x1Weights
   , gezira_GaussianBlur21x1
   , gezira_GaussianBlur1x21
   , gezira_LinearGradient
   , gezira_RadialGradient
   , gezira_PadGradient
   , gezira_RepeatGradient
   , gezira_ReflectGradient
   , gezira_ColorSpansBegin
   , gezira_ColorSpan
   , gezira_ColorSpansEnd
   , gezira_ApplyColorSpans
   , gezira_CompositeClear
   , gezira_CompositeSrc
   , gezira_CompositeDst
   , gezira_CompositeOver
   , gezira_CompositeDstOver
   , gezira_CompositeSrcIn
   , gezira_CompositeDstIn
   , gezira_CompositeSrcOut
   , gezira_CompositeDstOut
   , gezira_CompositeSrcAtop
   , gezira_CompositeDstAtop
   , gezira_CompositeXor
   , gezira_CompositePlus
   , gezira_CompositeMultiply
   , gezira_CompositeScreen
   , gezira_CompositeOverlay
   , gezira_CompositeDarken
   , gezira_CompositeLighten
   , gezira_CompositeColorDodge
   , gezira_CompositeColorBurn
   , gezira_CompositeHardLight
   , gezira_CompositeSoftLight
   , gezira_CompositeDifference
   , gezira_CompositeExclusion
   , gezira_CompositeSubtract
   , gezira_CompositeInvert
   , gezira_InverseOver
   , gezira_ContrastiveOver
   , gezira_Image_init
   , gezira_Image_done
   , gezira_Image_reset_gate
   , gezira_ReadFromImage_ARGB32
   , gezira_WriteToImage_ARGB32
   , gezira_CompositeUniformColorOverImage_ARGB32
   )

where
#include "gezira.h"
#include "gezira-image.h"
import Foreign.C
import Foreign
import Foreign.Marshal.Alloc
import System.IO.Unsafe
import qualified Data.ByteString as B

nileProcessFromC :: Ptr () -> NileProcess
nileProcessFromC p = NileProcess p

{- |
@
*nile_Process_t
@
-}
data NileProcess = NileProcess { nileProcessToC :: !(Ptr ()) }
data Pixels = Pixels { pixelsToC :: !(Ptr ())}
nileProcessNull :: NileProcess -> Bool
nileProcessNull (NileProcess ptr) = ptr == nullPtr

{- |
@
typedef struct {
    void           *pixels;
    int             width;
    int             height;
    int             stride;
    nile_Process_t *gate;
} gezira_Image_t;
@
-}
data GeziraImage = GeziraImage { geziraImageToC :: !(Ptr ()) } deriving Show

geziraImageSize :: Int
geziraImageSize = {#sizeof gezira_Image_t#}

geziraImageNull :: GeziraImage -> Bool
geziraImageNull image = nullPtr == (geziraImageToC image)

geziraImageGate :: GeziraImage -> IO (Maybe NileProcess)
geziraImageGate image = do
  gate <- {#get gezira_Image_t->gate #} (geziraImageToC image)
  return (if (gate == nullPtr) then Nothing else (Just (NileProcess gate)))

geziraImageSetGate :: GeziraImage -> NileProcess -> IO ()
geziraImageSetGate (GeziraImage ptr) gate = {#set gezira_Image_t->gate #} ptr (nileProcessToC gate)

geziraImageSetBounds :: GeziraImage -> CInt -> CInt -> IO ()
geziraImageSetBounds (GeziraImage ptr) width height = do
  {#set gezira_Image_t->width #} ptr width
  {#set gezira_Image_t->height #} ptr height

geziraImagePixels :: GeziraImage -> IO B.ByteString
geziraImagePixels (GeziraImage ptr) = do
  width <- {#get gezira_Image_t->width #} ptr
  height <- {#get gezira_Image_t->height #} ptr
  pixelsPtr <- {#get gezira_Image_t->pixels #} ptr
  let numPixels = width * height * (fromIntegral (sizeOf (undefined :: CUInt)))
  B.packCStringLen (castPtr pixelsPtr, fromIntegral numPixels)

{- |
@
nile_Process_t *
gezira_TransformBeziers (nile_Process_t *p,
                         float v_M_a,
                         float v_M_b,
                         float v_M_c,
                         float v_M_d,
                         float v_M_e,
                         float v_M_f);
@
-}
{#fun gezira_TransformBeziers {
                         nileProcessToC `NileProcess'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ClipBeziers (nile_Process_t *p,
                    float v_min_x,
                    float v_min_y,
                    float v_max_x,
                    float v_max_y);
@
-}
{#fun gezira_ClipBeziers {
                           nileProcessToC `NileProcess'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                         } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CalculateBounds (nile_Process_t *p);
@
-}
{#fun gezira_CalculateBounds {
                               nileProcessToC `NileProcess'
                             } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_OffsetBezier (nile_Process_t *p,
                     float v_o,
                     float v_Z_A_x,
                     float v_Z_A_y,
                     float v_Z_B_x,
                     float v_Z_B_y,
                     float v_Z_C_x,
                     float v_Z_C_y);
@
-}
{#fun gezira_OffsetBezier {
                            nileProcessToC `NileProcess'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_MiterJoin (nile_Process_t *p,
                  float v_o,
                  float v_l,
                  float v_P_x,
                  float v_P_y,
                  float v_u_x,
                  float v_u_y,
                  float v_v_x,
                  float v_v_y);
@
-}
{#fun gezira_MiterJoin {
                         nileProcessToC `NileProcess'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                       } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_RoundJoin (nile_Process_t *p,
                  float v_o,
                  float v_P_x,
                  float v_P_y,
                  float v_u_x,
                  float v_u_y,
                  float v_v_x,
                  float v_v_y);
@
-}
{#fun gezira_RoundJoin {
                         nileProcessToC `NileProcess'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                       } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_JoinBeziers (nile_Process_t *p,
                    float v_o,
                    float v_l,
                    float v_Zi_A_x,
                    float v_Zi_A_y,
                    float v_Zi_B_x,
                    float v_Zi_B_y,
                    float v_Zi_C_x,
                    float v_Zi_C_y,
                    float v_Zj_A_x,
                    float v_Zj_A_y,
                    float v_Zj_B_x,
                    float v_Zj_B_y,
                    float v_Zj_C_x,
                    float v_Zj_C_y);
@
-}
{#fun gezira_JoinBeziers {
                           nileProcessToC `NileProcess'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CapBezier (nile_Process_t *p,
                  float v_o,
                  float v_c,
                  float v_Z_A_x,
                  float v_Z_A_y,
                  float v_Z_B_x,
                  float v_Z_B_y,
                  float v_Z_C_x,
                  float v_Z_C_y);
@
-}
{#fun gezira_CapBezier {
                          nileProcessToC `NileProcess'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                         ,`CFloat'
                       } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_OffsetAndJoin (nile_Process_t *p,
                      float v_o,
                      float v_l,
                      float v_c,
                      float v_Z1_A_x,
                      float v_Z1_A_y,
                      float v_Z1_B_x,
                      float v_Z1_B_y,
                      float v_Z1_C_x,
                      float v_Z1_C_y,
                      float v_Zi_A_x,
                      float v_Zi_A_y,
                      float v_Zi_B_x,
                      float v_Zi_B_y,
                      float v_Zi_C_x,
                      float v_Zi_C_y);
@
-}
{#fun gezira_OffsetAndJoin {
                             nileProcessToC `NileProcess'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                            ,`CFloat'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_StrokeOneSide (nile_Process_t *p,
                      float v_w,
                      float v_l,
                      float v_c);
@
-}
{#fun gezira_StrokeOneSide {
                            nileProcessToC `NileProcess'
                          ,`CFloat'
                          ,`CFloat'
                          ,`CFloat'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ReverseBeziers (nile_Process_t *p);
@
-}
{#fun gezira_ReverseBeziers {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_SanitizeBezierPath (nile_Process_t *p);
@
-}
{#fun gezira_SanitizeBezierPath {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_StrokeBezierPath (nile_Process_t *p,
                         float v_w,
                         float v_l,
                         float v_c);
@
-}
{#fun gezira_StrokeBezierPath {
                                nileProcessToC `NileProcess'
                               ,`CFloat'
                               ,`CFloat'
                               ,`CFloat'
                              } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_DecomposeBeziers (nile_Process_t *p);
@
-}
{#fun gezira_DecomposeBeziers {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CombineEdgeSamples (nile_Process_t *p);
@
-}
{#fun gezira_CombineEdgeSamples {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_Rasterize (nile_Process_t *p);
@
-}
{#fun gezira_Rasterize {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_RectangleSpans (nile_Process_t *p,
                       float v_min_x,
                       float v_min_y,
                       float v_max_x,
                       float v_max_y);
@
-}
{#fun gezira_RectangleSpans {
                              nileProcessToC `NileProcess'
                             ,`CFloat'
                             ,`CFloat'
                             ,`CFloat'
                             ,`CFloat'
                            } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_TransformPoints (nile_Process_t *p,
                        float v_M_a,
                        float v_M_b,
                        float v_M_c,
                        float v_M_d,
                        float v_M_e,
                        float v_M_f);
@
-}
{#fun gezira_TransformPoints {
                               nileProcessToC `NileProcess'
                              ,`CFloat'
                              ,`CFloat'
                              ,`CFloat'
                              ,`CFloat'
                              ,`CFloat'
                              ,`CFloat'
                             } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_PadTexture (nile_Process_t *p,
                   float v_D_x,
                   float v_D_y);
@
-}
{#fun gezira_PadTexture {
                          nileProcessToC `NileProcess'
                         ,`CFloat'
                         ,`CFloat'
                        } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_RepeatTexture (nile_Process_t *p,
                      float v_D_x,
                      float v_D_y);
@
-}
{#fun gezira_RepeatTexture {
                            nileProcessToC `NileProcess'
                           ,`CFloat'
                           ,`CFloat'
                           } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ReflectTexture (nile_Process_t *p,
                       float v_D_x,
                       float v_D_y);
@
-}
{#fun gezira_ReflectTexture {
                              nileProcessToC `NileProcess'
                             ,`CFloat'
                             ,`CFloat'
                            } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_UniformColor (nile_Process_t *p,
                     float v_C_a,
                     float v_C_r,
                     float v_C_g,
                     float v_C_b);
@
-}
{#fun gezira_UniformColor {
                            nileProcessToC `NileProcess'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                           ,`CFloat'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeTextures (nile_Process_t *p,
                          nile_Process_t *v_t1,
                          nile_Process_t *v_t2,
                          nile_Process_t *v_c);
@
-}
{#fun gezira_CompositeTextures {
                                 nileProcessToC `NileProcess'
                               , nileProcessToC `NileProcess'
                               , nileProcessToC `NileProcess'
                               , nileProcessToC `NileProcess'
                               } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ExpandSpans (nile_Process_t *p);
@
-}
{#fun gezira_ExpandSpans {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ExtractSamplePoints (nile_Process_t *p);
@
-}
{#fun gezira_ExtractSamplePoints {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ApplyTexture (nile_Process_t *p,
                     nile_Process_t *v_t);
@
-}
{#fun gezira_ApplyTexture {
                            nileProcessToC `NileProcess'
                          , nileProcessToC `NileProcess'
                          } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_SumWeightedColors (nile_Process_t *p,
                          float v_n);
@
-}
{#fun gezira_SumWeightedColors {
                                 nileProcessToC `NileProcess'
                               ,`CFloat'
                               } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BilinearFilterPoints (nile_Process_t *p);
@
-}
{#fun gezira_BilinearFilterPoints {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BilinearFilterWeights (nile_Process_t *p);
@
-}
{#fun gezira_BilinearFilterWeights {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BilinearFilter (nile_Process_t *p,
                       nile_Process_t *v_t);
@
-}
{#fun gezira_BilinearFilter {
                              nileProcessToC `NileProcess'
                            , nileProcessToC `NileProcess'
                            } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BicubicFilterPoints (nile_Process_t *p);
@
-}
{#fun gezira_BicubicFilterPoints {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BicubicFilterDeltas (nile_Process_t *p);
@
-}
{#fun gezira_BicubicFilterDeltas {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BicubicFilterWeights (nile_Process_t *p);
@
-}
{#fun gezira_BicubicFilterWeights {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_BicubicFilter (nile_Process_t *p,
                      nile_Process_t *v_t);
@
-}
{#fun gezira_BicubicFilter {
                             nileProcessToC `NileProcess'
                           , nileProcessToC `NileProcess'
                           } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur5x1Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur5x1Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x5Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur1x5Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur5x1Weights (nile_Process_t *p,
                               float v_f);
@
-}
{#fun gezira_GaussianBlur5x1Weights {
                                      nileProcessToC `NileProcess'
                                     ,`CFloat'
                                    } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur5x1 (nile_Process_t *p,
                        float v_f,
                        nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur5x1 {
                               nileProcessToC `NileProcess'
                              ,`CFloat'
                              , nileProcessToC `NileProcess'
                             } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x5 (nile_Process_t *p,
                        float v_f,
                        nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur1x5 {
                               nileProcessToC `NileProcess'
                              ,`CFloat'
                              , nileProcessToC `NileProcess'
                             } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur11x1Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur11x1Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x11Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur1x11Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur11x1Weights (nile_Process_t *p,
                                float v_f);
@
-}
{#fun gezira_GaussianBlur11x1Weights {
                                       nileProcessToC `NileProcess'
                                      ,`CFloat'
                                     } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur11x1 (nile_Process_t *p,
                         float v_f,
                         nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur11x1 {
                               nileProcessToC `NileProcess'
                              ,`CFloat'
                              , nileProcessToC `NileProcess'
                              } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x11 (nile_Process_t *p,
                         float v_f,
                         nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur1x11 {
                                nileProcessToC `NileProcess'
                               ,`CFloat'
                               , nileProcessToC `NileProcess'
                              } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur21x1Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur21x1Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x21Points (nile_Process_t *p);
@
-}
{#fun gezira_GaussianBlur1x21Points {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur21x1Weights (nile_Process_t *p,
                                float v_f);
@
-}
{#fun gezira_GaussianBlur21x1Weights {
                                      nileProcessToC `NileProcess'
                                     ,`CFloat'
                                     } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur21x1 (nile_Process_t *p,
                         float v_f,
                         nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur21x1 {
                                nileProcessToC `NileProcess'
                               ,`CFloat'
                               , nileProcessToC `NileProcess'
                              } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_GaussianBlur1x21 (nile_Process_t *p,
                         float v_f,
                         nile_Process_t *v_t);
@
-}
{#fun gezira_GaussianBlur1x21 {
                                nileProcessToC `NileProcess'
                               ,`CFloat'
                               , nileProcessToC `NileProcess'
                              } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_LinearGradient (nile_Process_t *p,
                       float v_S_x,
                       float v_S_y,
                       float v_E_x,
                       float v_E_y);
@
-}
{#fun gezira_LinearGradient {
                              nileProcessToC `NileProcess'
                             ,`CFloat'
                             ,`CFloat'
                             ,`CFloat'
                             ,`CFloat'
                            } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_RadialGradient (nile_Process_t *p,
                       float v_C_x,
                       float v_C_y,
                       float v_r);
@
-}
{#fun gezira_RadialGradient {
                              nileProcessToC `NileProcess'
                             ,`CFloat'
                             ,`CFloat'
                             ,`CFloat'
                            } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_PadGradient (nile_Process_t *p);
@
-}
{#fun gezira_PadGradient {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_RepeatGradient (nile_Process_t *p);
@
-}
{#fun gezira_RepeatGradient {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ReflectGradient (nile_Process_t *p);
@
-}
{#fun gezira_ReflectGradient {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ColorSpansBegin (nile_Process_t *p);
@
-}
{#fun gezira_ColorSpansBegin {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ColorSpan (nile_Process_t *p,
                  float v_S1_a,
                  float v_S1_r,
                  float v_S1_g,
                  float v_S1_b,
                  float v_S2_a,
                  float v_S2_r,
                  float v_S2_g,
                  float v_S2_b,
                  float v_l);
@
-}
{#fun gezira_ColorSpan {
                         nileProcessToC `NileProcess'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                        ,`CFloat'
                       } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ColorSpansEnd (nile_Process_t *p);
@
-}
{#fun gezira_ColorSpansEnd {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}


{- |
@
nile_Process_t *
gezira_ApplyColorSpans (nile_Process_t *p,
                        nile_Process_t *v_spans);
@
-}
{#fun gezira_ApplyColorSpans {
                               nileProcessToC `NileProcess'
                             , nileProcessToC `NileProcess'
                             } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeClear (nile_Process_t *p);
@
-}
{#fun gezira_CompositeClear {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSrc (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSrc {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDst (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDst {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeOver (nile_Process_t *p);
@
-}
{#fun gezira_CompositeOver {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDstOver (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDstOver {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSrcIn (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSrcIn {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDstIn (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDstIn {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSrcOut (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSrcOut {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDstOut (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDstOut {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSrcAtop (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSrcAtop {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDstAtop (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDstAtop {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeXor (nile_Process_t *p);
@
-}
{#fun gezira_CompositeXor {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositePlus (nile_Process_t *p);
@
-}
{#fun gezira_CompositePlus {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeMultiply (nile_Process_t *p);
@
-}
{#fun gezira_CompositeMultiply {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeScreen (nile_Process_t *p);
@
-}
{#fun gezira_CompositeScreen {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeOverlay (nile_Process_t *p);
@
-}
{#fun gezira_CompositeOverlay {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDarken (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDarken {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeLighten (nile_Process_t *p);
@
-}
{#fun gezira_CompositeLighten {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeColorDodge (nile_Process_t *p);
@
-}
{#fun gezira_CompositeColorDodge {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeColorBurn (nile_Process_t *p);
@
-}
{#fun gezira_CompositeColorBurn {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeHardLight (nile_Process_t *p);
@
-}
{#fun gezira_CompositeHardLight {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSoftLight (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSoftLight {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeDifference (nile_Process_t *p);
@
-}
{#fun gezira_CompositeDifference {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeExclusion (nile_Process_t *p);
@
-}
{#fun gezira_CompositeExclusion {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeSubtract (nile_Process_t *p);
@
-}
{#fun gezira_CompositeSubtract {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeInvert (nile_Process_t *p);
@
-}
{#fun gezira_CompositeInvert {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_InverseOver (nile_Process_t *p,
                    float v_A);
@
-}
{#fun gezira_InverseOver {
                            nileProcessToC `NileProcess'
                          ,`CFloat'
                         } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_ContrastiveOver (nile_Process_t *p,
                        float v_a);
@
-}
{#fun gezira_ContrastiveOver {
                               nileProcessToC `NileProcess'
                              ,`CFloat'
                             } -> `NileProcess' nileProcessFromC #}



{- |
@
void
gezira_Image_init (gezira_Image_t *image, void *pixels, int width, int height, int stride);
@
-}
{#fun gezira_Image_init as gezira_Image_init'
                        {
                          id `Ptr ()'
                        , id `Ptr ()'
                        , `CInt'
                        , `CInt'
                        , `CInt'
                        } -> `()' #}
gezira_Image_init :: CInt -> CInt -> CInt -> IO GeziraImage
gezira_Image_init width height stride = do
   pixelMem <- mallocBytes (fromIntegral (width * height) * sizeOf (undefined :: CUInt))
   imageMem <- mallocBytes {#sizeof gezira_Image_t#}
   gezira_Image_init' imageMem pixelMem width height stride
   return (GeziraImage imageMem)

{- |
@
void
gezira_Image_done (gezira_Image_t *image);
@
-}
{#fun gezira_Image_done { geziraImageToC `GeziraImage' } -> `()' #}


{- |
@
void
gezira_Image_reset_gate (gezira_Image_t *image);
@
-}
{#fun gezira_Image_reset_gate { geziraImageToC `GeziraImage' } -> `()' #}

{- |
@
nile_Process_t *
gezira_ReadFromImage_ARGB32 (nile_Process_t *p, gezira_Image_t *image, int skipNextGate);
@
-}
{#fun gezira_ReadFromImage_ARGB32 {
                                    nileProcessToC `NileProcess'
                                  , geziraImageToC `GeziraImage'
                                  , `CInt'
                                  } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_WriteToImage_ARGB32 (nile_Process_t *p, gezira_Image_t *image);
@
-}
{#fun gezira_WriteToImage_ARGB32 {
                                   nileProcessToC `NileProcess'
                                 , geziraImageToC `GeziraImage'
                                 } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
gezira_CompositeUniformColorOverImage_ARGB32 (nile_Process_t *p, gezira_Image_t *image,
                                              float a, float r, float g, float b);
@
-}
{#fun gezira_CompositeUniformColorOverImage_ARGB32 {
                                                     nileProcessToC `NileProcess'
                                                   , geziraImageToC `GeziraImage'
                                                   , `CFloat'
                                                   , `CFloat'
                                                   , `CFloat'
                                                   , `CFloat'
                                                   } -> `NileProcess' nileProcessFromC #}
